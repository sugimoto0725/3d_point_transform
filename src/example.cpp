#include <geometry_msgs/Point.h>
#include <geometry_msgs/PoseArray.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
// PCL specific includes
#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/time_synchronizer.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <pcl/common/transforms.h>
#include <pcl/filters/passthrough.h>
#include <pcl_conversions/pcl_conversions.h>
#include <sugi_msgs/RectNamedArray.h>
#include <sugi_msgs/RectNamedArrayStamped.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <vector>
#include <stdlib.h>

ros::Publisher pub;
const float focal_length = 678.12780;//focal length of stereo_camera

float GetPointDistance(const sensor_msgs::PointCloud2ConstPtr cloud, int x,
                       int y)
{
  //  int index = x * cloud->row_step / cloud->height
  //              + y * cloud->point_step / cloud->width;
  int index = y * cloud->row_step + x * cloud->point_step;

  int z_offset = cloud->fields[2].offset;
  float z = 0;
  memcpy(&z, &cloud->data[index + z_offset], sizeof(z));
  return z;
}

void GetPointXYZ(const sensor_msgs::PointCloud2ConstPtr cloud, int x, int y,
                 std::vector<float>& point)
{
  const int point_dim = 3;//0:x 1:y 2:z
  point.resize(point_dim);
  int index = y * cloud->row_step + x * cloud->point_step;
  std::vector<uint32_t> offsets(point_dim);
  offsets[0] = cloud->fields[0].offset;
  offsets[1] = cloud->fields[1].offset;
  offsets[2] = cloud->fields[2].offset;
  for (size_t i = 0; i < offsets.size(); ++i) {
    memcpy(&point[i], &cloud->data[index + offsets[i]], sizeof(float));
  }
}

bool Comparing2DVectorsX(const std::vector<float>& left, const std::vector<float>& right)
{
  
  return (left[0]<right[0]);
}
bool Comparing2DVectorsY(const std::vector<float>& left, const std::vector<float>& right)
{
  
  return (left[1]<right[1]);
}
bool Comparing2DVectorsZ(const std::vector<float>& left, const std::vector<float>& right)
{
  
  return (left[2]<right[2]);
}

void _2DVectorSort(const std::vector<std::vector<float> >& src, std::vector<std::vector<float> >& dist)
{
  std::vector<std::vector<float> > tmp_vec;
  tmp_vec = src;
  std::sort(tmp_vec.begin(), tmp_vec.end(), Comparing2DVectorsX);
  for (int i=0; i<tmp_vec.size();i++){
    dist[0].push_back(tmp_vec[i][0]);
  }

  std::sort(tmp_vec.begin(), tmp_vec.end(), Comparing2DVectorsY);
  for (int i=0; i<tmp_vec.size();i++){
    dist[1].push_back(tmp_vec[i][1]);
  }

  std::sort(tmp_vec.begin(), tmp_vec.end(), Comparing2DVectorsZ);
  for (int i=0; i<tmp_vec.size();i++){
    dist[2].push_back(tmp_vec[i][2]);
  }

}

void StdSort(const std::vector<float>& src, std::vector<float>& dist)
{
  dist = src;
  std::stable_sort(dist.begin(), dist.end());
}

void SortSelection(const std::vector<float>& src, std::vector<float>& dist)
{
  dist = src;
  for (size_t i = 0; i < dist.size(); ++i) {
    float id = i;
    float min_val = std::numeric_limits<float>::max();
    for (size_t j = i; j < dist.size(); ++j) {
      if (dist[j] < min_val) {
        id = j;
        min_val = dist[j];
      }
    }
    std::swap(dist[i], dist[id]);
  }
}

void sugi_sort(const std::vector<float>& src, std::vector<float>& dist)
{
  std::vector<float> raw_dist = src;
  std::vector<float>& sorted_dist = dist;
  std::vector<float>::iterator raw_dist_itr = raw_dist.begin();
  std::vector<float>::iterator mini_index_itr;

  int rotate_num = static_cast<int>(raw_dist.size());
  for (int i = 0; i < rotate_num; i++) {
    float mini_dist = std::numeric_limits<float>::max();
    while (raw_dist_itr != raw_dist.end()) {
      if (*raw_dist_itr <= mini_dist) {
        mini_dist = *raw_dist_itr;
        mini_index_itr = raw_dist_itr;
      }
      raw_dist_itr++;
    }
    sorted_dist.push_back(mini_dist);
    raw_dist.erase(mini_index_itr);
    raw_dist_itr = raw_dist.begin();
  }
}

float LimitExtract(const sensor_msgs::PointCloud2ConstPtr& cloud,
                   const cv::Rect& rect_info, float (&max)[3], float (&min)[3])
{
  int cx = rect_info.x + rect_info.width / 2;
  int cy = rect_info.y + rect_info.height / 2;
  int x_range = rect_info.width;
  int y_range = rect_info.height;
  std::vector<float> point;
  for (int i = 0; i <= 2; i++) {
    float maxmum = -1;
    float minimum = 1000;
    for (int x = rect_info.x; x <= rect_info.x + x_range; ++x) {
      for (int y = rect_info.y; y <= rect_info.y + y_range; ++y) {
        GetPointXYZ(cloud, x, y, point);
        if (!std::isnan(point[i])) {
          if (point[i] > maxmum) maxmum = point[i];
          if (point[i] < minimum) minimum = point[i];
        }
      }
    }
    max[i] = maxmum;
    min[i] = minimum;
    //    std::cout<<"max_func: "<<max[i]<<std::endl;
  }
}

void ShowPointXYZ2D(const sensor_msgs::PointCloud2ConstPtr cloud,
                    const sugi_msgs::RectNamed& rect_info)
{
  cv::Rect cv_rect_info;
  cv_rect_info.height = rect_info.rect.height;
  cv_rect_info.width = rect_info.rect.width;
  cv_rect_info.x = rect_info.rect.x;
  cv_rect_info.x = rect_info.rect.y;
  std::vector<float> point;
  for (int i = 0; i <= 2; i++) {
    cv::Mat img(cv::Size(cv_rect_info.width, cv_rect_info.height), CV_8UC1);
    img = cv::Scalar::all(0);
    //    std::string black_name;
    //    if(i==0)      black_name="xblack.jpg";
    //    else if(i==1) black_name="yblack.jpg";
    //    else if(i==2) black_name="zblack.jpg";
    //    cv::imwrite(rect_info.name + black_name, img);
    float max[3], min[3];  //[0]:x, [1]:y, [2]:z
    LimitExtract(cloud, cv_rect_info, max, min);
    std::cout << "Flag1" << std::endl;
    
    for (int y = cv_rect_info.y+cv_rect_info.height; y < cv_rect_info.y+cv_rect_info.height;
         y++) {
      for (int x = cv_rect_info.x+cv_rect_info.width; x < cv_rect_info.x + cv_rect_info.width;
           x++) {
        GetPointXYZ(cloud, x, y, point);

        if (!std::isnan(point[i])) {
          img.at<uchar>(y - cv_rect_info.y, x - cv_rect_info.x) =
              static_cast<uchar>(255 * (point[i] - min[i]) / (max[i] - min[i]));
        } else {
          img.at<uchar>(y - cv_rect_info.y, x - cv_rect_info.x) = 0;
          // std::cout<<"Flag--nonnum---------------------"<<std::endl;
        }
      }
      //      if(point[i]>max[i])
      //      {
      //        //std::cout<<"pixel:
      //        "<<((point[i]-min[i])/(max[i]-min[i]))<<std::endl;
      //        std::cout << "max: " << max[i] << std::endl;
      //        std::cout << "pixel: " << point[i] << std::endl;
      //      }
    }
    std::cout << "width: " << cv_rect_info.width << std::endl;
    std::cout << "height: " << cv_rect_info.height << std::endl;
    std::cout << "size: " << img.size() << std::endl;
    std::cout << "i=" << i << std::endl;
    std::string img_name;
    if (i == 0)
      img_name = "x.jpg";
    else if (i == 1)
      img_name = "y.jpg";
    else if (i == 2)
      img_name = "z.jpg";
    std::cout << "Flag1--------------------------" << std::endl;
    //    cv::imshow("aaa.jpg"/*rect_info.name + img_name*/, img);
    //    cv::waitKey(5000);
    cv::imwrite(rect_info.name + img_name, img);
    std::cout << "Flag2--------------------------" << std::endl;
  }
}

bool NanCheck(const std::vector<float>& xyz)
{
  const int point_dim = 3;//0:x 1:y 2:z
  for(int i=0; i<point_dim; i++)
  {
    if (std::isnan(xyz[i]))
    {
      return false;
    }
  }
  return true;
}

int GetRandom(int min,int max)
{
  return min + (int)(rand()*(max-min+1.0)/(1.0+RAND_MAX));
}

void ShowVector(const std::vector<float>& vec)
{
  for(int i = 0; i < vec.size(); i++){
    for(std::vector<float>::const_iterator itr = vec.begin(); itr != vec.end(); ++itr){
      std::cout<< *itr<<std::endl;
    }
  }
}

void ShowVector(const std::vector<std::vector<float> >& vec)
{
  for(int i = 0; i <vec.size(); i++){
    for(std::vector<float>::const_iterator itr = vec[i].begin(); itr != vec[i].end(); itr++){
      std::cout<< *itr <<std::endl;
    }
  }
}

void CreateTestVector(std::vector<float>& test, std::vector<std::vector<float> >& tests)
{
  const int point_dim = 3;//0:x 1:y 2:z
  const int number_of_point = 10;
  test.resize(point_dim);
  int max=200, min=0;
  for(int i=0; i<number_of_point; i++){
    for(int j=0; j<point_dim; j++){
      test[j]= static_cast<float>(GetRandom(min,max));
    }
    tests.push_back(test);
  }
}

void GetPointDistanceAverage(const sensor_msgs::PointCloud2ConstPtr cloud,
                             const sugi_msgs::RectNamed& rect_info,
                             std::vector<float>& point)
{
  int cx = rect_info.rect.x + rect_info.rect.width / 2;
  int cy = rect_info.rect.y + rect_info.rect.height / 2;
  int x_range = rect_info.rect.width;
  int y_range = rect_info.rect.height;
  float z_sum = 0.0;
  int cnt = 0;
  std::vector<float> xyz; //0:x 1:y 2:z
  std::vector<std::vector<float> > xyz_vec;
  const double width_rate = 0.20; //make width narrow
  //  for (int x = -x_range / 2; x < x_range / 2; ++x) {
  //    for (int y = -y_range / 2; y < y_range / 2; ++y) {
  for (int x = static_cast<int>(-x_range*width_rate / 2); x <
    static_cast<int>(x_range * width_rate / 2); ++x)
  {
    for (int y = -y_range / 2; y < y_range / 2; ++y)
    {
      int tx = cx + x;  // target x position[pix]
      int ty = cy + y;  // target y position[pix]
      if (cloud->width > tx && tx > 0 && cloud->height > ty && ty > 0)
      {
        //        float GetPointDistance(cloud, tx, ty);
        GetPointXYZ(cloud, tx, ty, xyz);
        bool nan_check = NanCheck(xyz); //0:includeng nan 1:no nan
        if (nan_check)xyz_vec.push_back(xyz);
      }
    }
    
  }

  for (size_t i = 0; i < xyz_vec.size(); ++i)
  {
    z_sum += xyz_vec[i][2];//xyz_vec[i][2] is z(depth) of point
  }
  //  {
  //    ShowPointXYZ2D(cloud, rect_info);
  //  }
  std::vector<std::vector<float> > sorted_dist;
  //  sorted_dist.resize(xyz_vec.size());
  const int point_dim = 3;//0:x 1:y 2:z
  std::vector<float> median_value(point_dim); //0:x 1:y 2:z
  std::vector<float>::const_iterator itr;
  
//  CreateTestVector(std::vector<float> test, std::vector<std::vector<float> > tests);

  sorted_dist.resize(xyz_vec[0].size());
  _2DVectorSort(xyz_vec, sorted_dist);
  
  for(int i=0; i<sorted_dist.size(); i++) {
    if (sorted_dist[i].size() % 2 == 0) {
      itr = sorted_dist[i].begin() + sorted_dist[i].size() / 2;
      median_value[i] = *itr;
    } else {
      itr = sorted_dist[i].begin() + (sorted_dist[i].size() / 2 - 0.5);
      median_value[i] = (*itr + *(itr + 1)) / 2;
    }
  }

  point.resize(median_value.size());
//  for(int i=0; i<3; i++){
//    point[i] = median_value[i];
//  }
  int half_width = cloud->width / 2;
  int half_height = cloud->height / 2;
//  point[0] = median_value[2] / focal_length * (cx - half_width);
//  point[1] = median_value[2] / focal_length * (cy - half_height);
//  point[2] = median_value[2];
  point[0] = (z_sum/xyz_vec.size()) / focal_length * (cx - half_width);
  point[1] = (z_sum/xyz_vec.size()) / focal_length * (cy - half_height);
  point[2] = z_sum/xyz_vec.size();
  std::cout<<"dist: "<<z_sum/xyz_vec.size()<<std::endl;
  
}

void GetPointDistanceMedian(const sensor_msgs::PointCloud2ConstPtr cloud,
                  const sugi_msgs::RectNamed& rect_info,
                  std::vector<float>& point)
{
  int cx = rect_info.rect.x + rect_info.rect.width / 2;
  int cy = rect_info.rect.y + rect_info.rect.height / 2;
  int x_range = rect_info.rect.width;
  int y_range = rect_info.rect.height;
  float z_sum = 0.0;
  int cnt = 0;
  float dist;
  std::vector<float> dist_vec;
  for (int x = -x_range / 2; x < x_range / 2; ++x)
  {
    for (int y = -y_range / 2; y < y_range / 2; ++y)
    {
      int tx = cx + x;  // target x position[pix]
      int ty = cy + y;  // target y position[pix]
      if (cloud->width > tx && tx > 0 && cloud->height > ty && ty > 0) {
        dist=GetPointDistance(cloud, tx, ty);
        if(!isnan(dist)) dist_vec.push_back(dist);
      }
    }
  }
  std::vector<float> sorted_dist;
  StdSort(dist_vec,sorted_dist);
  std::vector<float>::const_iterator itr;
  float depth;
  if (sorted_dist.size() % 2 == 0) {
    itr = sorted_dist.begin() + sorted_dist.size() / 2;
    depth = *itr;
  } else {
    itr = sorted_dist.begin() + (sorted_dist.size() / 2 - 0.5);
    depth = (*itr + *(itr + 1)) / 2;
  }
  int half_width = cloud->width / 2;
  int half_height = cloud->height / 2;
  point[0] = depth / focal_length * (cx - half_width);
  point[1] = depth / focal_length * (cy - half_height);
  point[2] = depth;
  
}

void GetTfMatrix(Eigen::Matrix4d& tf_matrix, const std::string& base_name,
                 const std::string& target_name)
{
  // get transform matrix
  static tf::TransformListener listener;
  tf::StampedTransform transform;

  ros::Time now = ros::Time(0);
  try {
    listener.waitForTransform(base_name, target_name, now, ros::Duration(1.0));
    listener.lookupTransform(base_name, target_name, now, transform);
  } catch (tf::TransformException ex) {
    ROS_ERROR("%s", ex.what());
    ros::Duration(1.0).sleep();
    return;
  }
  tf::Matrix3x3 rot(transform.getRotation());
  tf::Vector3 vec = transform.getOrigin();
  tf_matrix = Eigen::Matrix4d::Identity();
  {
    Eigen::Matrix3d mat_tmp;
    tf::matrixTFToEigen(rot, mat_tmp);
    tf_matrix.block(0, 0, 3, 3) = mat_tmp;
  }
  {
    Eigen::Vector3d vec_tmp;
    tf::vectorTFToEigen(vec, vec_tmp);
    tf_matrix.block(0, 3, 3, 1) = vec_tmp;
  }
}

void cloud_cb(const sensor_msgs::PointCloud2ConstPtr& cloud,
              const sugi_msgs::RectNamedArrayStampedConstPtr& rect_info)
{
  // Perform the actual filtering
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_pcl(
      new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::fromROSMsg(*cloud, *cloud_pcl);

  // get transform matrix
  Eigen::Matrix4d mat_eigen = Eigen::Matrix4d::Identity();
  std::string base_name = "base_link";
  std::string target_name = "left_camera";
  GetTfMatrix(mat_eigen, base_name, target_name);
  //  std::cout << mat_eigen << std::endl;

  // get objects region
  std::vector<float> point(3);
  for (size_t i = 0; i < rect_info->rects.size(); ++i) {
    const sugi_msgs::RectNamed& item = rect_info->rects[i];
    GetPointDistanceAverage(
        cloud, item,
        point);  // point...0:x, 1:y, 2:z
//    GetPointDistanceMedian(
//      cloud, item,
//      point);  // point...0:x, 1:y, 2:z
    float x_img = point[0];
    float y_img = point[1];
    float z_img = point[2];

    std::cout << "object: " << rect_info->rects[i].name << std::endl;
    //    std::cout << "x: " << point[0] << std::endl;
    //    std::cout << "y: " << point[1] << std::endl;
    //    std::cout << "obj_width: " << point[2] << std::endl;
    //    std::cout << "obj_height: " << point[3] << std::endl;
    //    std::cout << "z: " << point[4]<< std::endl;

    // convert image(x,y,z) to world(X,Y,Z)
    Eigen::Vector4f vec_in_world;
    {
      Eigen::Vector4f vec_in_image;
      vec_in_image[0] = x_img;
      vec_in_image[1] = y_img;
      vec_in_image[2] = z_img;
      vec_in_image[3] = 1.0;
      Eigen::Matrix4f tmp = mat_eigen.cast<float>();
      vec_in_world = tmp * vec_in_image;
    }

    std::cout << "x_world: " << vec_in_world[0] << std::endl;
    std::cout << "y_world: " << vec_in_world[1] << std::endl;
//    std::cout << "z_world: " << vec_in_world[2] << std::endl;
//    std::cout << "z_world: " << vec_in_world[2] << std::endl;
    std::cout << "---------------------" << std::endl;

    // transform
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformed_cloud(
        new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::transformPointCloud(*cloud_pcl, *transformed_cloud, mat_eigen);

    visualization_msgs::Marker marker;
    {
      marker.header.frame_id = base_name;
      marker.header.stamp = rect_info->header.stamp;
      marker.type = visualization_msgs::Marker::SPHERE;
      marker.pose.position.x = vec_in_world[0];
      marker.pose.position.y = vec_in_world[1];
      marker.pose.position.z = 0;
      marker.scale.x = 0.05;
      marker.scale.y = 0.05;
      marker.scale.z = 0.05;
      marker.color.a = 1.0;
      marker.color.r = 1.0;
      marker.color.g = 0.0;
      marker.color.b = 0.0;
      marker.ns = item.name;
    }

    //    extract region of object using filter
    //    pcl::PassThrough<pcl::PointXYZRGB> ptfilter(
    //      true);
    //    ptfilter.setInputCloud(transformed_cloud);
    //    ptfilter.setFilterFieldName("x");
    //    ptfilter.setFilterLimits(vec_in_world[0], vec_in_world[0]+obj_width);
    //    ptfilter.filter(*transformed_cloud);
    //    ptfilter.setFilterFieldName("y");
    //    ptfilter.setFilterLimits(vec_in_world[1]-obj_width/2,
    //    vec_in_world[1]+obj_width/2);
    //    ptfilter.filter(*transformed_cloud);
    //    ptfilter.setFilterFieldName("z");
    //    ptfilter.setFilterLimits(vec_in_world[2]-obj_height/2,
    //    vec_in_world[2]+obj_height/2);
    //    ptfilter.filter(*transformed_cloud);
    //
    //    //  // Publish the data-
    //    pcl::PCLPointCloud2 tmp;
    //    pcl::toPCLPointCloud2(*transformed_cloud, tmp);
    //
    //    sensor_msgs::PointCloud2 cloud_filtered;
    //    pcl_conversions::fromPCL(tmp, cloud_filtered);
    //    cloud_filtered.header.frame_id = base_name;
    //    pub.publish(cloud_filtstdio.h ered);

    pub.publish(marker);
  }
}
// 1.5707963267948966
int main(int argc, char** argv)
{
  //  std::cout << "test" << std::endl;
  // Initialize ROS
  ros::init(argc, argv, "my_pcl_tutorial");
  ros::NodeHandle nh;
  ROS_INFO("node has started--");

  message_filters::Subscriber<sensor_msgs::PointCloud2> poi_sub(nh, "poi_input",
                                                                5);
  message_filters::Subscriber<sugi_msgs::RectNamedArrayStamped> obj_sub(
      nh, "obj_input", 5);
  typedef message_filters::sync_policies::
      ApproximateTime<sensor_msgs::PointCloud2,
                      sugi_msgs::RectNamedArrayStamped>
          MySyncPolicy;
  message_filters::Synchronizer<MySyncPolicy> sync(MySyncPolicy(5), poi_sub,
                                                   obj_sub);
  sync.registerCallback(boost::bind(&cloud_cb, _1, _2));
  pub = nh.advertise<visualization_msgs::Marker>("visualization_marker", 1);
  //  pub = nh.advertise<sensor_msgs::PointCloud2_>("output", 100);
  ros::Rate loop_rate(1);
  while (ros::ok()) {
    ros::spinOnce();
    loop_rate.sleep();
  }
}
