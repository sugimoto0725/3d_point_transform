#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
// PCL specific includes
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <tf/transform_listener.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/transforms.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>


ros::Publisher pub;

void
cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud)
{
  sensor_msgs::PointCloud2 cloud_filtered;
  //
  //  // Perform the actual filtering
  pcl::PassThrough<pcl::PointXYZRGB> ptfilter (true);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_pcl(new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::fromROSMsg(*cloud, *cloud_pcl);
  //  pcl::VoxelGrid<pcl::PointXYZRGB> sor;
  ptfilter.setInputCloud(cloud_pcl);
  ptfilter.setFilterFieldName("x");
  ptfilter.setFilterLimits(1, 2);
  ptfilter.filter(*cloud_pcl);
  //  sor.setInputCloud (cloud_pcl);
  //  sor.setLeafSize (0.01, 0.01, 0.01);
  //  sor.filter (*cloud_pcl);
  //  pcl::PCLPointCloud2 tmp;
  //  pcl::toPCLPointCloud2(*cloud_pcl, tmp);
  //  pcl_conversions::fromPCL(tmp, cloud_filtered);
  cloud_filtered.header = cloud->header;
  //  // Publish the data
  pcl::PCLPointCloud2 tmp;
  pcl::toPCLPointCloud2(*cloud_pcl, tmp);
  pcl_conversions::fromPCL(tmp, cloud_filtered);
  ROS_INFO("transformed");
  pub.publish (cloud_filtered);
}

int main (int argc, char** argv)
{
  // Initialize ROS
  ROS_INFO("node has started--");
  ros::init (argc, argv, "my_pcl_tutorial");
  ros::NodeHandle nh;
  
  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("input", 1, cloud_cb);
  
  // Create a ROS publisher for the output point cloud
  pub = nh.advertise<sensor_msgs::PointCloud2> ("output", 1);
  
  // Spin
  ros::spin ();
}
